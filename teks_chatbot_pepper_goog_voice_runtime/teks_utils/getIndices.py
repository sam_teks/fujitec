#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import requests
import urllib
import json
import datetime
import re

def getIndices():
	url = "http://www.aastocks.com/en/mobile/Indices.aspx"
	raw = urllib.urlopen(url)
	raw = str(raw.read())

	startpos = raw.find('<!-- HK Index -->')
	#endpos = raw.find('<!-- China Index -->')
	endpos = raw.find('GEM')
	raw = raw[startpos:endpos]

	raw = re.sub('/[ ]{2,}|[\t]/', ' ',raw)
	raw = re.sub('/[ ]{1,}|[\t]/', ' ',raw)
	raw = raw.replace(' ', '')
	raw = raw.replace('  ', '')
	raw = raw.replace('<br/>', '')
	raw = raw.replace('\n', '')
	raw = raw.replace('\r', '')
	raw = raw.replace('\r\n', '')
	raw = raw.replace('&nbsp;', '')
	raw = raw.replace('<spanclass="posbold">', '');
	raw = raw.replace('<spanclass="negbold">', '');
	raw = raw.replace('<spanclass="uncbold">', '');
	raw = raw.replace('<tdclass="fortynumbottomtext_right>"', '');
	raw = re.sub('/\r|\n/', '', raw)
	raw = re.sub('[\s+]', '', raw)

	#print raw

	#hsi_data
	hsi_startpos = raw.find('<tdclass="fortybottom">HSI</td><tdclass="twentynumbottomtext_right">')
	hsi_endpos = raw.find('<tdclass="fortybottom">HSCEI</td>')
	hsi_data = raw[hsi_startpos:hsi_endpos]
	#print hsi_data

	#$HSI
	hsi_startpos = hsi_data.find('<tdclass="fortybottom">HSI</td><tdclass="twentynumbottomtext_right">') + 68
	hsi_endpos = hsi_data.find('</td><tdclass="fortynumbottomtext_right">')
	hsi = hsi_data[hsi_startpos:hsi_endpos]
	#print hsi

	#hsi pos neg
	hsipn_startpos = hsi_data.find('</td><tdclass="fortynumbottomtext_right">') + 41
	hsipn_endpos = hsi_data.find('</span>(')
	hsipn = hsi_data[hsipn_startpos:hsipn_endpos]
	#print hsipn

	#hscei
	hscei_startpos = raw.find('<tdclass="fortybottom">HSCEI</td><tdclass="twentynumbottomtext_right">')
	hscei_endpos = raw.find('</td></tr><tr><tdclass="fortybottom">HSRed-chip</td>')
	hscei_data = raw[hscei_startpos:hscei_endpos]
	#print hscei_data

	#hscei
	hscei_startpos = hscei_data.find('<tdclass="fortybottom">HSCEI</td><tdclass="twentynumbottomtext_right">') + 70
	hscei_endpos = hscei_data.find('</td><tdclass="fortynumbottomtext_right">')
	hscei = hscei_data[hscei_startpos:hscei_endpos]
	#print hscei

	#hscei pos neg
	hsceipn_startpos = hscei_data.find('</td><tdclass="fortynumbottomtext_right">') + 41
	hsceipn_endpos = hscei_data.find('</span>(')
	hsceipn = hscei_data[hsceipn_startpos:hsceipn_endpos]
	#print hsceipn

	hsi = str(int(float(hsi.replace(',', ''))))
	hsipn = (int(float(hsipn.replace(',', ''))))
	hscei = str(int(float(hscei.replace(',', ''))))
	hsceipn = (int(float(hsceipn.replace(',', ''))))

	if (hsipn == 0):
		hsimg = "無起跌"
	elif (hsipn > 0):
		hsimg = "升：" + str(hsipn) + " 點。"
	else:
		hsimg = "跌：" + str(abs(hsipn)) + " 點。"

	if (hsceipn == 0):
		hsceimg = "無起跌"
	elif (hsceipn > 0):
		hsceimg = "升：" + str(hsceipn) + " 點。"
	else:
		hsceimg = "跌：" + str(abs(hsceipn)) + " 點。"

	msg = "恒生指數：" + hsi + " 點。" + hsimg + "國企指數：" + hscei + " 點。" + hsceimg
	print msg
	return json.dumps([hsi, hsipn, hscei, hsceipn])

def main():
	cd = getIndices()
	print cd
	json_cd = json.loads(cd)
	hsi = json_cd[0]
	hsipn = json_cd[1]
	hscei = json_cd[2]
	hsceipn = json_cd[3]
	from datetime import datetime
	curdate = "{:%Y %m %d %H:%M:%S}".format(datetime.now())
	json_cd.append(curdate)
	print json.dumps(json_cd)
	

if __name__ == "__main__":
	main()
