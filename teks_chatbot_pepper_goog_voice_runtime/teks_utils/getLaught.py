# -*- coding: utf-8 -*-
import os
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import urllib 
import parse
import json
import datetime
import re
from suds.client import Client
from suds.sudsobject import asdict
from random import randint

def recursive_translation(d):
	result = {}
	for k, v in asdict(d).iteritems():
		if hasattr(v, '__keylist__'):
			result[k] = recursive_translation(v)
			result[k] = result[k]
		elif isinstance(v, list):
			result[k] = []
			for item in v:
				if hasattr(item, '__keylist__'):
					result[k].append(recursive_translation(item))
				else:
					result[k].append(item)
		else:
			result[k] = v
	return result

def getlaught():
	maxresult=50
	page=1
	showapi_appid=34736
	showapi_test_draft="false"
	showapi_timestamp=datetime.datetime.now().strftime("%Y%m%d%H%M%S")
	showapi_sign="a868004ef5d64b8d945016e354852a1f"

	url="http://route.showapi.com/341-1?maxResult=" + str(maxresult) + "&page=" + str(page) + "&showapi_appid=" + str(showapi_appid) + "&showapi_test_draft=" + showapi_test_draft + "&showapi_timestamp=" + str(showapi_timestamp) + "&showapi_sign=" + showapi_sign
	raw = urllib.urlopen(url)
	raw = str(raw.read())

	if 'showapi_res_body' in raw:
		raw = re.sub('/[ ]{2,}|[\t]/', ' ',raw)
		raw = re.sub('/[ ]{1,}|[\t]/', ' ',raw)
		raw = raw.replace(' ', '')
		raw = raw.replace('  ', '')
		raw = raw.replace('<br/>', '')
		raw = raw.replace(r"\r\n", r"")
		raw = raw.replace('\n', '')
		raw = raw.replace('\r', '')
		raw = raw.replace('\r\n', '')
		raw = raw.replace('&nbsp;', '')
		raw = re.sub('/\r|\n/', '', raw)
		raw = re.sub('[\s+]', '', raw)

		randNum = (randint(0,49))

		item_dict = json.loads(raw)
		story = (item_dict['showapi_res_body']['contentlist'][randNum]['text'])

		jsondata = '{"Errorcode":0,"story":"' + str(story) + '"}'		
	else:
		jsondata = '{"Errorcode":0}'		

	return jsondata
	
def main():
	raw = getlaught()

	print raw

if __name__ == "__main__":
	main()
