import urllib2
import json

def getNews(t):
	#url = "https://api.rss2json.com/v1/api.json?rss_url=http://rthk9.rthk.hk/rthk/news/rss/c_expressnews_csport.xml"
	url = "https://api.rss2json.com/v1/api.json?rss_url=http://rthk9.rthk.hk/rthk/news/rss/c_expressnews_c"+t+".xml"
	response = urllib2.urlopen(url)
	data = json.loads(response.read());
	return data

def main():
	data = getNews("sport")
	print '====== ' + data['status'] + ' ======';

	for item in data['items']:
		print item['title'].encode("utf-8")

	data = getNews("international")
	print '====== ' + data['status'] + ' ======';

	for item in data['items']:
		print item['title'].encode("utf-8")
		
	data = getNews("local")
	print '====== ' + data['status'] + ' ======';

	for item in data['items']:
		print item['title'].encode("utf-8")
		
if __name__ == "__main__":
	main()	