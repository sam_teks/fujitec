
#!/usr/bin/env python
#  -*- coding:utf-8 -*-
import pdb
import os
import subprocess
import sys
import time
import json
import requests
import urllib2
import urllib
import threading
import datetime
from teks_utility import *
#from teks_asr_ifly import teks_record_thread
import neo4jtools.teks_neo4j_func as db_tools
import neo4jtools.teks_authorize_neo4j as neo4j_graph
from teks_speech_moves import teks_speech_moves_thread
from secret_words import *

import logging
import teks_logger as teks_log
DEFAULT_LOGGER_FORMATTER = logging.Formatter('%(message)s,%(levelname)s,"%(asctime)s"')
DEFAULT_LOGGER_LEVEL = logging.INFO
logger = teks_log.teks_logger("teks_demo", \
                     DEFAULT_LOGGER_FORMATTER, \
                     None, \
                     DEFAULT_LOGGER_LEVEL)
def main():
    for arg in sys.argv[1:]:
        print arg
    value = sys.argv[1]
    graph = neo4j_graph.authorize_dialog()
    print "neo4j searching answers"
    ans_in_yue = db_tools.search_answer(graph, value, logger, language='CT')
if __name__ == '__main__':
    main()

