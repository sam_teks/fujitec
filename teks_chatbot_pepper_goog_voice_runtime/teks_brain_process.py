# -*- coding:utf-8 -*-


import os
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

import re
import time
import codecs
import shutil
import unicodecsv
import collections

import teks_configure as CONFIGURE

################################################################################

BRAIN_FOLDER_NAME_TO_LANGUAGE = {
  "cn": CONFIGURE.LANG_CN,
  "ct": CONFIGURE.LANG_CT,
  "en": CONFIGURE.LANG_EN
}

LANGUAGE_TO_BRAIN_FOLDER_NAME = {
  CONFIGURE.LANG_CN: "cn", 
  CONFIGURE.LANG_CT: "ct", 
  CONFIGURE.LANG_EN: "en"
}

################################################################################

def get_path_modified_time(path):

  return int(os.stat(path).st_mtime)

def get_root_path_folder_path_as_list(root_path):

  dir_list = os.listdir(root_path)
  path_list = []
  for dir in dir_list:
    path = os.path.join(root_path, dir)
    if os.path.isdir(path):
      path_list.append(path)
  
  return path_list
  
def get_root_path_folder_path_as_dict(root_path):

  dir_list = os.listdir(root_path)
  path_dict = {}
  for dir in dir_list:
    path = os.path.join(root_path, dir)
    if os.path.isdir(path):
      path_dict[dir] = path
  
  return path_dict
  
def get_root_path_file_path_as_dict(root_path):

  dir_list = os.listdir(root_path)
  path_dict = {}
  for dir in dir_list:
    path = os.path.join(root_path, dir)
    if os.path.isfile(path):
      path_dict[dir] = path
  
  return path_dict
  
################################################################################

def get_brain_name():

  brain_root_path = CONFIGURE.RIVESCRIPT_BRAIN_ROOT_PATH
  brain_lang_path_dict = get_root_path_folder_path_as_dict(brain_root_path)
  
  brain_name = {}
  try:
    for (lang_dir, path) in brain_lang_path_dict.items():
      brain_path_dict = get_root_path_folder_path_as_dict(path)
      brain_name[lang_dir] = brain_path_dict.keys()
  except Exception as e:
    print "\n *** ERROR TO GET RIVE BRAIN PATH:", e
  
  return brain_name

def get_brain_path():

  brain_root_path = CONFIGURE.RIVESCRIPT_BRAIN_ROOT_PATH
  brain_lang_path_dict = get_root_path_folder_path_as_dict(brain_root_path)
  
  brain_path = {}
  try:
    for (lang_dir, path) in brain_lang_path_dict.items():
      if lang_dir not in BRAIN_FOLDER_NAME_TO_LANGUAGE:
        continue
      brain_path[BRAIN_FOLDER_NAME_TO_LANGUAGE[lang_dir]] = get_root_path_folder_path_as_list(path)
  except Exception as e:
    print "\n *** ERROR TO GET RIVE BRAIN PATH:", e
  
  return brain_path

def get_brain_modified_time():
  
  brain_root_path = CONFIGURE.RIVESCRIPT_BRAIN_ROOT_PATH
  modified_time_dict = {}
  for (dirpath, dirnames, filenames) in os.walk(brain_root_path):
    for dirname in dirnames:
      path = os.path.join(dirpath, dirname)
      if path:
        modified_time_dict[path] = get_path_modified_time(path)
    for filename in filenames:
      path = os.path.join(dirpath, filename)
      if path:
        modified_time_dict[path] = get_path_modified_time(path)
        
  return modified_time_dict
  
def check_brain_modified_time(modified_time_dict):
  
  modified_flag = False
  current_modified_time_dict = get_brain_modified_time()
  if len(current_modified_time_dict.values()) != len(modified_time_dict.values()):
    print "#1"
    modified_flag =  True
  else:
    for path, modified_time in current_modified_time_dict.items(): 
      if not modified_time_dict.has_key(path):
        print "#2"
        modified_flag =  True
        break
      if modified_time_dict[path] != modified_time:
        print "#3: "+str(modified_time_dict[path])+" / "+str(modified_time)
        modified_flag =  True
        modified_time_dict[path] = modified_time
        break
        
  return modified_flag
  
def copy_path_to_another_path(from_path, dest_path):
  
  if not os.path.exists(from_path):
    return
    
  if os.path.exists(dest_path):
    shutil.rmtree(dest_path)
    
  shutil.copytree(from_path, dest_path)
  
def move_path_to_another_path(from_path, dest_path):
  
  if not os.path.exists(from_path):
    return
    
  if os.path.exists(dest_path):
    shutil.rmtree(dest_path)
    
  shutil.move(from_path, dest_path)
  
def remove_path(path):
  
  if not os.path.exists(path):
    return
  
  if os.path.exists(path):
    shutil.rmtree(path)
    
def copy_brain_python_file_to_another_path(from_path, dest_path):
  
  if not os.path.exists(from_path):
    return
  
  if not os.path.exists(dest_path):
    os.makedirs(dest_path)
  
  file_dict = get_root_path_file_path_as_dict(from_path)
  for (filename, filepath) in file_dict.items():
    if is_python_rive_file(filepath):
      shutil.copy(filepath, dest_path)
    if is_chatbot_inherent_rive_file(filepath):
      shutil.copy(filepath, dest_path)
  
################################################################################

def get_rive_type_from_text(text):
  
  input_text = text.lstrip().rstrip()
  output_dict = {"rive_type": "", "rive_text": ""}
  result = re.match(r"[\+\-\*\^\>\<]", input_text)
  if result:
    output_dict["rive_type"] = result.group(0)
    if result.group(0) == ">" or result.group(0) == "<":
      if "topic" in text:
        output_dict["rive_text"] = re.sub(r"(\>|\<|\s|topic)", "", text).strip()
    else:
      output_dict["rive_text"] = input_text.lstrip(result.group(0)).lstrip()
    
  return output_dict
  
def get_topic_name_from_text(text):

  input_text = text.lstrip().rstrip()
  next_topic = "dafault"
  
  result = re.search(r"\{(topic=).*?\}", input_text)
  if result:
    topic_name = re.sub(r"\{|\}|topic=", "", result.group(0)).lstrip().rstrip()
    if topic_name:
      next_topic = topic_name
    
  return next_topic
  
def remove_topic_name_from_text(text):

  input_text = text.lstrip().rstrip()
  output_text = re.sub(r"\{(topic=).*?\}", "", input_text).lstrip().rstrip()

  return output_text

def get_rive_dict_from_rive_file(filepath):

  rive_dict = collections.OrderedDict()
  current_question = ""
  current_answer = ""
  current_topic = "random"
  
  with open(filepath, "rb") as f:
  
    for line in f:
    
      rive_type_dict = get_rive_type_from_text(line)
      if (rive_type_dict["rive_text"] == "*" 
        or rive_type_dict["rive_text"] == "联系手动退出话题{topic=random}" 
        or rive_type_dict["rive_text"] == "歹势我寻不到答复联系其他大脑"):
        continue
      if rive_type_dict["rive_type"] == ">" and rive_type_dict["rive_text"]:
        current_topic = rive_type_dict["rive_text"]
        current_question = ""
        current_answer = ""
        rive_dict[current_topic] = collections.OrderedDict()
      if rive_type_dict["rive_type"] == "<":
        current_topic = "random"
        current_question = ""
        current_answer = ""
      
      if current_topic == "random":
        if rive_type_dict["rive_type"] == "+":
          current_question = rive_type_dict["rive_text"]
          if not rive_dict.has_key(current_question):
            rive_dict[current_question] = []
        if (rive_type_dict["rive_type"] == "-" or rive_type_dict["rive_type"] == "*"):
          current_answer = rive_type_dict["rive_text"]
          rive_dict[current_question].append(current_answer)
            
      if current_topic != "random":
        if rive_type_dict["rive_type"] == "+":
          current_question = rive_type_dict["rive_text"]
          if not rive_dict[current_topic].has_key(current_question):
            rive_dict[current_topic][current_question] = []
        if (rive_type_dict["rive_type"] == "-" or rive_type_dict["rive_type"] == "*"): 
          current_answer = rive_type_dict["rive_text"]
          rive_dict[current_topic][current_question].append(current_answer)
          
  return rive_dict
  
def get_raw_record_list_from_rive_file(filepath):

  record_list = []
  record = ["", []]  # [question, [answers]]
  
  with open(filepath, "rb") as f:
  
    for line in f:
    
      rive_type_dict = get_rive_type_from_text(line)
      
      if (rive_type_dict["rive_type"] == "+"):
        if record[0]:
          record_list.append(record)
          record = ["", []]
        record[0] = rive_type_dict["rive_text"]
      elif (rive_type_dict["rive_type"] == ">" or rive_type_dict["rive_type"] == "<"):
        if record[0]:
          record_list.append(record)
          record = ["", []]
        record = ["", []]
        record[0] = rive_type_dict["rive_text"]
        record_list.append(record)
        record = ["", []]
      elif (rive_type_dict["rive_type"] == "-" or rive_type_dict["rive_type"] == "*"):
        if "联系手动退出话题{topic=random}" in rive_type_dict["rive_text"]:
          record[0] = ""
          record[1] = []
        else:
          record[1].append(rive_type_dict["rive_text"])
      elif rive_type_dict["rive_type"] == "^":
        record[1][-1] += rive_type_dict["rive_text"]
  
  '''
  for record in record_list:
    print record[0], record[1]
  '''
  
  return record_list

def format_rive_dict_to_record_list(rive_dict):
  
  def format_rive_item_as_record_list(rive_item, current_topic):
    (key, value) = rive_item
    record_list = []
    question = format_rive_question_to_csv(key.decode("utf-8"))
    for v in value:
      answer = v.decode("utf-8")
      next_topic = get_topic_name_from_text(answer)
      if next_topic == "dafault":
        next_topic = current_topic
      answer = remove_topic_name_from_text(answer)
      record_list.append([question, answer, current_topic, next_topic])
    return record_list
   
  record_list = []
  
  for (key, value) in rive_dict.items():
    
    if isinstance(value, list):
      record_list.extend(format_rive_item_as_record_list((key, value), "random"))
    if isinstance(value, dict):
      current_topic = key.decode("utf-8")
      for (k, v) in value.items():
        if isinstance(v, list):   
          record_list.extend(format_rive_item_as_record_list((k, v), current_topic))
  
  '''
  for record in record_list:
    if "<get arrival_hall> == A => 接机大堂" in record[1]:
      print record[0], record[1], record[2], record[3]
      exit()
  '''
  
  return record_list

def format_rive_question_to_csv(text):
  
  input_text = text
  output_text = re.sub(r"(\(\|\*\))|(\(\*\|\))", "*", input_text)  # replace (|*) or (*|) to *
  return output_text
  
def format_csv_question_to_rive(text):

  input_text = text.lower()  # All lower is import!
  output_text = re.sub(r"\*", "(|*)", input_text)  # replace * to (|*)
  output_text = "+ %s" % output_text
  return output_text
  
def format_csv_answer_to_rive(text):

  input_text = text
  output_text = input_text.lstrip('"').rstrip('"')
  result = re.search(r"\=\>", input_text)
  if result:
    output_text = "* %s" % output_text
  else:
    output_text = "- %s" % output_text
  
  return output_text

def get_topic_flow_from_record(record):
  
  topic_flow = {"current_topic": "random", "next_topic": "random", "topic_flow_type": "random-random"}
  
  current_topic = ""
  next_topic = ""
  if len(record) == 3:
    current_topic = record[2]
  elif len(record) == 4:
    current_topic = record[2]
    next_topic = record[3]
  
  if not current_topic:
    current_topic = "random"
  if not next_topic:
    next_topic = "random"
    
  if current_topic == "random" and next_topic == "random":
    topic_flow["current_topic"] = "random"
    topic_flow["next_topic"] = "random"
    topic_flow["topic_flow_type"] = "random-random"
  if current_topic == "random" and next_topic != "random":
    topic_flow["current_topic"] = "random"
    topic_flow["next_topic"] = next_topic
    topic_flow["topic_flow_type"] = "random-topic"
  if current_topic != "random" and next_topic != "random":
    topic_flow["current_topic"] = current_topic
    topic_flow["next_topic"] = next_topic
    topic_flow["topic_flow_type"] = "topic-topic"
  if current_topic != "random" and next_topic == "random":
    topic_flow["current_topic"] = current_topic
    topic_flow["next_topic"] = "random"
    topic_flow["topic_flow_type"] = "topic-random"
    
  return topic_flow

def is_python_rive_file(filename):
  
  result = re.search(r"\.(python)\.rive", filename)
  if result:
    return True
  else:
    return False
    
def is_chatbot_inherent_rive_file(filename):
  
  result = re.search(r".*?[/\\]\-[0-9].*?\.rive", filename)
  if not result:
    return True
  else:
    return False

def export_csv_file(data_list, filepath):
  
  with open(filepath, "wb") as f:
    writer = unicodecsv.writer(f)
    for data in data_list:
      writer.writerow(data)
    
def convert_rive_to_csv_record_file(from_path, dest_path):
  
  record_list = []
  
  brain_path = from_path
  if not os.path.exists(brain_path):
    return
  
  start_time = time.time()
  
  rive_dict = collections.OrderedDict()
  brain_path_dict = get_root_path_file_path_as_dict(brain_path)
  for (dir, path) in brain_path_dict.items():
    if not is_python_rive_file(dir):
      rive_dict.update(get_rive_dict_from_rive_file(path))
  
  record_list = format_rive_dict_to_record_list(rive_dict)
  
  if not os.path.exists(dest_path):
    os.makedirs(dest_path)
  
  file_name = "-%s.csv" % int(time.time())
  file_path = os.path.join(dest_path, file_name)
  export_csv_file(record_list, file_path)
  
  print "\nconvert (%s) rive to (%s) csv time: %s" % (from_path, dest_path, time.time() - start_time)
      
def load_csv_file(filepath):
  
  data_list = []
  with open(filepath, "rb") as f:
    reader = unicodecsv.reader(f)
    for row in reader:
      data_list.append(row)
      
  return data_list

def format_record_as_dict(record):
  
  record_dict = {}
  if not record:
    return record_dict
  
  question = format_csv_question_to_rive(record[0])
  answer = format_csv_answer_to_rive(record[1])
  topic_flow = get_topic_flow_from_record(record)
  
  if topic_flow["topic_flow_type"] == "random-topic":
    answer = "%s{topic=%s}" % (answer, topic_flow["next_topic"])
  if topic_flow["topic_flow_type"] == "topic-topic":  # to other topic
    if topic_flow["current_topic"] != topic_flow["next_topic"]:
      answer = "%s{topic=%s}" % (answer, topic_flow["next_topic"])
  if topic_flow["topic_flow_type"] == "topic-random":  # to random topic
    answer = "%s{topic=%s}" % (answer, topic_flow["next_topic"])
    
  record_dict["question"] = question
  record_dict["answer"] = answer
  record_dict["topic_flow"] = topic_flow
  
  return record_dict
  
def format_record_list_to_rive_dict(record_list):
  
  rive_dict = collections.OrderedDict()
  
  for record in record_list:
      
    record_dict = format_record_as_dict(record)
    if not record_dict:
      continue
    
    question = record_dict["question"]
    answer = record_dict["answer"]
    current_topic = record_dict["topic_flow"]["current_topic"]

    if current_topic == "random":
      if not rive_dict.has_key(question):
        rive_dict[question] = []
      rive_dict[question].append(answer)
    if current_topic != "random":
      current_topic_key = "> topic %s" % current_topic
      if not rive_dict.has_key(current_topic_key):
        rive_dict[current_topic_key] = collections.OrderedDict()
      if not rive_dict[current_topic_key].has_key(question):
        rive_dict[current_topic_key][question] = []
      rive_dict[current_topic_key][question].append(answer)
      
  for (key, value) in rive_dict.items():
    
    if isinstance(value, dict):
      rive_dict[key]["+ *"] = ["- 联系手动退出话题{topic=random}"]
      rive_dict[key]["< topic"] = []
      
  '''
  for (key, value) in rive_dict.items():
    print key, value
  '''

  rive_dict["+ *"] = ["- 歹势我寻不到答复联系其他大脑"]
  return rive_dict 
  
def export_rive_file(rive_dict, filepath):
  
  def format_rive_item_as_rive_list(rive_item):
    (key, value) = rive_item
    rive_list = []
    text = ("\n" + key + "\n").decode("utf-8")
    rive_list.append(text)
    for v in value:
      text = (v + "\n").decode("utf-8")
      rive_list.append(text)
    return rive_list
    
  rive_list = ["! version = 2.0\n"]
  
  for (key, value) in rive_dict.items():
  
    if isinstance(value, list):
      rive_list.extend(format_rive_item_as_rive_list((key, value)))
    if isinstance(value, dict):
      text = ("\n" + key + "\n").decode("utf-8")
      rive_list.append(text)
      for (k, v) in value.items():
        if isinstance(v, list):
          rive_list.extend(format_rive_item_as_rive_list((k, v)))
  
  with codecs.open(filepath, "wb", "utf-8") as f:
    f.writelines(rive_list)
    
def convert_csv_record_to_rive_file(from_path, dest_path):

  brain_path = from_path
  if not os.path.exists(brain_path):
    return
  
  start_time = time.time()
  
  record_list = []
  brain_path_dict = get_root_path_file_path_as_dict(brain_path)
  for (dir, path) in brain_path_dict.items():
    if not is_python_rive_file(dir):
      record_list.extend(load_csv_file(path))
  
  '''
  for record in record_list:
    print record[0], record[1], record[2], record[3]
  '''
  
  rive_dict = format_record_list_to_rive_dict(record_list)
  
  if not os.path.exists(dest_path):
    os.makedirs(dest_path)
  
  file_name = "-%s.rive" % int(time.time())
  file_path = os.path.join(dest_path, file_name)
  export_rive_file(rive_dict, file_path)
  
  print "\nconvert (%s) csv to (%s) rive time: %s" % (from_path, dest_path, time.time() - start_time)
  
      
################################################################################

def add_prefix_to_brain_file(brain_root_path, prefix):
  
  for (dirpath, dirnames, filenames) in os.walk(brain_root_path):
    for filename in filenames:
      path = os.path.join(dirpath, filename)
      new_path = os.path.join(dirpath, prefix + filename)
      if path:
        os.rename(path, new_path)

def main():

  #convert_rive_to_csv_record_file("./brain/cn/test", "./administrator_data/test-csv/cn/test")
  '''
  convert_rive_to_csv_record_file("./brain/cn/airline", "./administrator_data/test-csv/cn/airline")
  convert_rive_to_csv_record_file("./brain/cn/facility","./administrator_data/test-csv/cn/facility")
  convert_rive_to_csv_record_file("./brain/cn/flight_info","./administrator_data/test-csv/cn/flight_info")
  convert_rive_to_csv_record_file("./brain/cn/location","./administrator_data/test-csv/cn/location")
  convert_rive_to_csv_record_file("./brain/cn/miscellaneous_topic","./administrator_data/test-csv/cn/miscellaneous_topic")
  convert_rive_to_csv_record_file("./brain/ct/airline","./administrator_data/test-csv/ct/airline")
  convert_rive_to_csv_record_file("./brain/ct/facility","./administrator_data/test-csv/ct/facility")
  convert_rive_to_csv_record_file("./brain/ct/flight_info","./administrator_data/test-csv/ct/flight_info")
  convert_rive_to_csv_record_file("./brain/ct/location","./administrator_data/test-csv/ct/location")
  convert_rive_to_csv_record_file("./brain/ct/miscellaneous_topic","./administrator_data/test-csv/ct/miscellaneous_topic")
  convert_rive_to_csv_record_file("./brain/en/airline","./administrator_data/test-csv/en/airline")
  convert_rive_to_csv_record_file("./brain/en/facility","./administrator_data/test-csv/en/facility")
  convert_rive_to_csv_record_file("./brain/en/flight_info","./administrator_data/test-csv/en/flight_info")
  convert_rive_to_csv_record_file("./brain/en/location","./administrator_data/test-csv/en/location")
  convert_rive_to_csv_record_file("./brain/en/miscellaneous_topic","./administrator_data/test-csv/en/miscellaneous_topic")
  '''
  
  #convert_csv_record_to_rive_file("./administrator_data/test-csv/cn/test", "./administrator_data/test-rive/cn/test")
  '''
  convert_csv_record_to_rive_file("./administrator_data/test-csv/cn/airline", "./administrator_data/test-rive/cn/airline")
  convert_csv_record_to_rive_file("./administrator_data/test-csv/cn/facility","./administrator_data/test-rive/cn/facility")
  convert_csv_record_to_rive_file("./administrator_data/test-csv/cn/flight_info","./administrator_data/test-rive/cn/flight_info")
  convert_csv_record_to_rive_file("./administrator_data/test-csv/cn/location","./administrator_data/test-rive/cn/location")
  convert_csv_record_to_rive_file("./administrator_data/test-csv/cn/miscellaneous_topic","./administrator_data/test-rive/cn/miscellaneous_topic")
  convert_csv_record_to_rive_file("./administrator_data/test-csv/ct/airline","./administrator_data/test-rive/ct/airline")
  convert_csv_record_to_rive_file("./administrator_data/test-csv/ct/facility","./administrator_data/test-rive/ct/facility")
  convert_csv_record_to_rive_file("./administrator_data/test-csv/ct/flight_info","./administrator_data/test-rive/ct/flight_info")
  convert_csv_record_to_rive_file("./administrator_data/test-csv/ct/location","./administrator_data/test-rive/ct/location")
  convert_csv_record_to_rive_file("./administrator_data/test-csv/ct/miscellaneous_topic","./administrator_data/test-rive/ct/miscellaneous_topic")
  convert_csv_record_to_rive_file("./administrator_data/test-csv/en/airline","./administrator_data/test-rive/en/airline")
  convert_csv_record_to_rive_file("./administrator_data/test-csv/en/facility","./administrator_data/test-rive/en/facility")
  convert_csv_record_to_rive_file("./administrator_data/test-csv/en/flight_info","./administrator_data/test-rive/en/flight_info")
  convert_csv_record_to_rive_file("./administrator_data/test-csv/en/location","./administrator_data/test-rive/en/location")
  convert_csv_record_to_rive_file("./administrator_data/test-csv/en/miscellaneous_topic","./administrator_data/test-rive/en/miscellaneous_topic")
  add_prefix_to_brain_file("./administrator_data/test-rive/", "inherent")
  '''
  
  '''
  copy_path_to_another_path("./brain/cn/airline", "./administrator_data/xxxxxxxxx/cn/airline")
  move_path_to_another_path("./administrator_data/xxxxxxxxx/cn/airline", "./administrator_data/xxxxxxxxx/ct/airline1")
  remove_path("./administrator_data/xxxxxxxxx/cn/airline1")
  '''
  
  '''
  copy_brain_python_file_to_another_path("./brain/ct/flight_info", "./administrator_data/xxx")
  '''
  
if __name__ == "__main__":
  
  main()
  