# -*- coding: utf-8 -*-

import re
import sys
reload(sys)
sys.setdefaultencoding("utf-8")

from langconv import Converter

################################################################################

def is_chinese_text(text):
  input_text = text.decode("utf-8")
  result = re.search(ur"[\u3105-\u3120\u31c0-\u31e3\u3400-\u4db5\u4e00-\u9fa5\u9fa6-\u9fcb\u3007]+", input_text)
  if result:
    return True
  else:
    return False

class chinese_converter(object):

  def __init__(self):
    self.chk = Converter("zh-hant")
    self.ccn = Converter("zh-hans")

  def convert_hk(self, text):
    return self.chk.convert(text.decode("utf-8")).encode("utf-8")

  def convert_cn(self, text):
    return self.ccn.convert(text.decode("utf-8")).encode("utf-8")

################################################################################

if __name__ == "__main__":

  '''
  cc = chinese_converter()
  print "s2hk", cc.convert_hk("后边和皇后在测试中国我在这里为什么是嘅哪裏")
  print "hk2s", cc.convert_cn("后边和皇后我正在測試中國我在这里为什么是嘅邊度")
  '''
  
  print is_chinese_text("ABCD〇一二三四五六七八九十")
  