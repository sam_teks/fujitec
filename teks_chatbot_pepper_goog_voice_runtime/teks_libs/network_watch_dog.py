import time

try:
    import httplib
except:
    import http.client as httplib


######################################################
class NetworkWatchDog(object):

    def __init__(self,url,t,success_cb,fail_cb):
        self.url = url
        self.t = t
        self.success_cb = success_cb
        self.fail_cb = fail_cb
        pass

    def have_internet(self):
        conn = httplib.HTTPConnection(self.url, timeout=self.t)
        try:
            conn.request("HEAD", "/")
            conn.close()
            #print("**************have_internet ")
            if self.success_cb:
                self.success_cb()
            return True
        except:
            conn.close()
            print("**************No_internet ")
            if self.fail_cb:
                self.fail_cb()
            return False

