import time

######################################################
class PeopleTester(object):

    def __init__(self, service_cache, logger, collec_time = 0.5, time_interval = 0.05, firstzone = 1.5, secondzone = 3.0):
        self.s = service_cache
        self.logger = logger

        self.TIME = collec_time
        self.time_interval = time_interval
        self.FirstZone = firstzone
        self.SecondZone = secondzone

    def human_distance(self):
        self.currentDistance = 0  # Default furest distance is 2.0m
        self.distancesList = []
        try:
            self.s.ALEngagementZones.setSecondLimitDistance(self.SecondZone)
            self.s.ALEngagementZones.setFirstLimitDistance(self.FirstZone)
        except Exception as e:
            self.logger.info("=======I cannot set the zones distance======")
            self.logger.info(e)
            return 0
        #judge to give self.distance different value
        self.logger.info("*************in measureDistance***********")
        cur_time = 0
        while cur_time < self.TIME:
            self.currentDistance = self.s.ALMemory.getData("Device/SubDeviceList/Platform/Front/Sonar/Sensor/Value")
            self.distancesList.append(self.currentDistance)
            time.sleep(self.time_interval)
            cur_time += self.time_interval
        #self.logger.info(str(self.distancesList))
        return self.distance_deal(self.distancesList)

    def distance_deal(self,datas):
        z1 = 0
        z2 = 0
        z3 = 0
        for data in datas:
            if data < self.FirstZone:
                z1 += 1
            elif data < self.SecondZone and data > self.FirstZone:
                z2 += 1
            else:
                z3 += 1
        #self.logger.info("====="+str(z1)+"====="+str(z2)+"====="+str(z3))
        if z1 > max(z2,z3):
            return 1
        elif z2 > z3:
            return 2
        else:
            return 3
