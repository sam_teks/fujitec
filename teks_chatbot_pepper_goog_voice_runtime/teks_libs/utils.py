import time

def check_time_diff(past):
    now = time.time()
    return int(now - past)

'''class NoiseTester(object):
    def __init__(self, service_cache, logger, noise_threshold = 5000, collec_time=2, time_interval=0.05):
        # generic activity boilerplate
        self.s = service_cache
        self.logger = logger
        self.collec_time = collec_time
        self.time_interval = time_interval
        #NOISE_VAR = 800
        self.NOISE_MEAN = noise_threshold

    def __call__(self):
        times = 0
        noise_status = True
        x1 = []
        x2 = []
        x3 = []
        x4 = []
        self.s.ALAudioDevice.enableEnergyComputation()
        self.logger.info("*************in collecting energy***********")
        while times < self.collec_time:
            x1.append(self.s.ALAudioDevice.getFrontMicEnergy())
            x2.append(self.s.ALAudioDevice.getRearMicEnergy())
            x3.append(self.s.ALAudioDevice.getLeftMicEnergy())
            x4.append(self.s.ALAudioDevice.getRightMicEnergy())
            time.sleep(self.time_interval)
            times += self.time_interval
        energy_1 = self.mean(x1)
        energy_2 = self.mean(x2)
        energy_3 = self.mean(x3)
        energy_4 = self.mean(x4)
        Noise = [energy_1,energy_2,energy_3,energy_4]
        self.logger.info("=========="+str(Noise) + "===========")
        if not Noise:
            self.logger.error("cannot collec noise!!!")
            return noise_status
        else:
            mean = self.mean(Noise)
            if mean < self.NOISE_MEAN:
                noise_status = False
            else:
                noise_status = True
        self.logger.info("noise status is "+str(noise_status))
        return noise_status


    def variance(self,l):
        s1=0
        for i in l:
            s1+=i**2
        return int((int(s1/4)-(self.mean(l))**2)**0.5)

    def mean(self,l):
        sum = 0
        for i in l:
            sum += i
        return int(sum/len(l))'''
