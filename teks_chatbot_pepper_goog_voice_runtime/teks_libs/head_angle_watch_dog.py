import time


######################################################
class HeadAngleWatchDog(object):
    def __init__(self,s,target,ll,hl,cb):
        self.target = target
        self.ll = ll
        self.hl = hl
        self.cb = cb

        self.s = s
        self.app = self.s.ALMemory

        self.targetName = "Face"

        self.mode = "Head"
        self.width = 0.1

        self.distanceX = 0.3
        self.thresholdX = 0.1
        self.distanceY = 0.0
        self.thresholdY = 0.1
        self.angleWz = 0.0
        self.thresholdWz = 0.3

        self.effector = "None"
        self.isTracking = False

        pass

    def stop_face_tracker(self):
        if self.isTracking:
            self.s.ALTracker.stopTracker()
            self.isTracking = False

    def enable_face_tracker(self):
        if self.s:
            self.s.ALTracker.setEffector(self.effector)

            self.s.ALTracker.registerTarget(self.targetName, self.width)
            self.s.ALTracker.setRelativePosition([-self.distanceX, self.distanceY, self.angleWz,
                                               self.thresholdX, self.thresholdY, self.thresholdWz])
            self.s.ALTracker.setMode(self.mode)

            self.s.ALTracker.track(self.targetName) #Start tracker
            self.isTracking = True


    def restart_detection(self):
        if self.s:
            self.stop_face_tracker()

            self.s.ALMotion.setStiffnesses("Head", 1.0)
            self.s.ALMotion.setAngles("HeadYaw",0,0.2)
            self.enable_face_tracker()

    def process_head_angle(self):
        result = self.app.getData(self.target)
        #print(str(result))

        if not (self.ll <= result and result <= self.hl):
            print("OVER process head angle: "+self.target+":"+str(self.ll)+":"+str(self.hl))
            if self.cb:
                self.cb(result)

