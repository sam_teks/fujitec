#!/usr/bin/env python
#  -*- coding:utf-8 -*-

import sys
reload(sys)
sys.setdefaultencoding("utf8")

import time
#import unicodecsv
import uuid
import random
import json
import chardet
import logging
from naoqi import ALBroker
from naoqi import ALProxy
from naoqi import ALModule
import teks_configure as CONFIGURE
import re

import os

from zhtools import chinese_converter
chinese_conv = chinese_converter.chinese_converter()


'''
CURRENT_LANGUAGE_TO_IFLY_LANG_CODE = {CONFIGURE.LANG_CN: "zh_cn",
                    CONFIGURE.LANG_CT: "zh_hk",
                    CONFIGURE.LANG_EN: "en_us"}
'''                    
CURRENT_LANGUAGE_TO_GOOG_LANG_CODE = {CONFIGURE.LANG_CN: "cmn-Hans-HK",
                    CONFIGURE.LANG_CT: "yue-Hant-HK",
                    CONFIGURE.LANG_EN: "en-US"}

def match_concept(text, conceptWords):
    return any(word in text for word in conceptWords)

def exactmatch_concept(text, conceptWords):
    return any(word == text for word in conceptWords)

def write_pid_file(fname):
    cur_dir = os.getcwd()
    pid = str(os.getpid())
    f = open(fname, 'w')
    f.write(pid)
    f.close()

def group_words(s):
    regex = []

    # Match a whole word:
    regex += [ur'\w+']

    # Match a single CJK character:
    regex += [ur'[\u4e00-\ufaff]']

    # Match one of anything else, except for spaces:
    regex += [ur'[^\s]']

    regex = "|".join(regex)
    r = re.compile(regex)

    return r.findall(s)
    
def encrypt(string, length):
    return ' '.join(string[i:i+length] for i in xrange(0,len(string),length))

def spliteKeyWord(str):
    regex = r"[\u4e00-\ufaff]|[0-9]+|[a-zA-Z]+\'*[a-z]*"
    matches = re.findall(regex, str, re.UNICODE)
    return matches

def listen_text(record_thread, current_lang):
  # STT process
  record_thread.set_input(CURRENT_LANGUAGE_TO_GOOG_LANG_CODE[current_lang])
  record_thread.resume()
  while not record_thread.is_suspend(): time.sleep(0.01)
  record_thread.suspend()
  text = record_thread.get_output()
  speech_lang = record_thread.get_speech_lang()
  return text, speech_lang

def say_text(tts_thread, text, current_lang):
  # TTS process
  tts_thread.set_input(text, current_lang)
  tts_thread.resume()
  while not tts_thread.is_suspend(): time.sleep(0.01)
  tts_thread.suspend()


def check_language(text, current_lang):
  global chinese_conv

  lang = current_lang
  if chinese_converter.is_chinese_text(text):
    lang = CONFIGURE.LANG_CT
    '''
    if text == chinese_conv.convert_hk(text):
      lang = CONFIGURE.LANG_CT
    else:
      lang = CONFIGURE.LANG_CN
    '''
  else:
    lang = CONFIGURE.LANG_EN

  return lang


def check_current_language(text):

  text = text.lower()
  if "讲英文" in text or "讲英语" in text or "说英文" in text or "说英语" in text:
    return "CHINESE_TO_ENGLISH"
  if "讲广东话" in text or "说广东话" in text:
    return "CHINESE_TO_CANTONESE"
  if "speak chinese" in text:
    return "ENGLISH_TO_CHINESE"
  if "speak cantonese" in text:
    return "ENGLISH_TO_CANTONESE"
  if "講中文" in text or "讲中文" in text:
    return "CANTONESE_TO_CHINESE"
  if "講英文" in text or "講英語" in text or "說英文" in text or "說英語" in text:
    return "CANTONESE_TO_ENGLISH"
  return ""

def set_current_language(tts_thread, lang_setting, current_lang):

  lang = current_lang
  if lang_setting == "CHINESE_TO_ENGLISH" or lang_setting == "CANTONESE_TO_ENGLISH":
    lang = CONFIGURE.LANG_EN
    say_text(tts_thread, "I am now speak English!", lang)
  elif lang_setting == "CHINESE_TO_CANTONESE" or lang_setting == "ENGLISH_TO_CANTONESE":
    lang = CONFIGURE.LANG_CT
    say_text(tts_thread, "我依家講廣東話！", lang)
  elif lang_setting == "ENGLISH_TO_CHINESE" or lang_setting == "CANTONESE_TO_CHINESE":
    lang = CONFIGURE.LANG_CN
    say_text(tts_thread, "我现在讲普通话！", lang)

  return lang

def get_reply_from_json(text):

  reply_text = text
  try:
    json_data = json.loads(text)

    if json_data.has_key("reply_text"):
      reply_text = json_data["reply_text"]
      
      if json_data.has_key("reply_content"):
        reply_content = json_data["reply_content"]

        if len(reply_content) == 0:
          reply_text = str(reply_text)
        else:
          reply_text = str(reply_text[0])
         
      if json_data.has_key("recommend"):
        reply_text += json_data["recommend"]

  except Exception as e:
    print e

  reply_text = str(reply_text)
  
  return reply_text

def save_unmatched_sentence(unmatched_sentence_list):
  '''
  if len(unmatched_sentence_list) > 0:
    with open("./logs/unmatched_" + str(uuid.uuid1()) + ".log", 'wb') as f:
      writer = unicodecsv.writer(f, encoding='utf-8')
      for text in unmatched_sentence_list:
        writer.writerow([text])
  '''
  if len(unmatched_sentence_list) > 0:
    pass
    '''
    graph = neo4j.authorize_local()
    neo4j.save_unmatched_sentence_to_neo4j(graph, unmatched_sentence_list)
    '''

################################################################################

UNMATCHED_REPLY_TEXT_LIST_CN = ["怎么说呢", "我不太明白", "我不太理解"]
UNMATCHED_REPLY_TEXT_LIST_CT = ["唔好意思,唔該講多一次", "我唔知呀,我仲係學緊野,唔該问我同事","有什麼問題,唔該问问我同事"]
UNMATCHED_REPLY_TEXT_LIST_EN = ["唔好意思,唔該講多一次", "我唔知呀,我仲係學緊野,唔該问我同事","有什麼問題,唔該问问我同事"]
#UNMATCHED_REPLY_TEXT_LIST_EN = ["sorry, not understand", "please repeat", "I don't know", "please find other staff for help"]

def make_reply_natural(text, current_lang):
  reply_text = text

  if text == "":
    unmatched_reply_text_list = []
    if current_lang == CONFIGURE.LANG_EN:
      unmatched_reply_text_list = UNMATCHED_REPLY_TEXT_LIST_EN
    elif current_lang == CONFIGURE.LANG_CT:
      unmatched_reply_text_list = UNMATCHED_REPLY_TEXT_LIST_CT
    elif current_lang == CONFIGURE.LANG_CN:
      unmatched_reply_text_list = UNMATCHED_REPLY_TEXT_LIST_CN

    reply_text = random.choice(unmatched_reply_text_list)

  return reply_text

################################################################################

'''
nao_broker = ALBroker("nao_broker",
            "0.0.0.0",  # listen to anyone
            0,      # find a free port and use it
            CONFIGURE.NAO_ALPROXY_IP,      # parent broker IP
            CONFIGURE.NAO_ALPROXY_PORT)    # parent broker port
'''
class app_starter_module(object):
  def __init__(self, name):
    self.behavior_manager_proxy = ALProxy("ALBehaviorManager", CONFIGURE.NAO_ALPROXY_IP, CONFIGURE.NAO_ALPROXY_PORT)
    pass

  def start(self, app_name):
    try:
      print ("starting app: " + app_name)
      if not self.behavior_manager_proxy.isBehaviorRunning(app_name):
        self.behavior_manager_proxy.runBehavior(app_name)
      else:
        print ("app already running: " + app_name)
    except Exception as e:
      print (e)
    pass

  def start_aysn(self, app_name):
    try:
      print ("starting app: " + app_name)
      if not self.behavior_manager_proxy.isBehaviorRunning(app_name):
        self.behavior_manager_proxy.post.runBehavior(app_name)
      else:
        print ("app already running: " + app_name)
    except Exception as e:
      print (e)
    pass

  def stop(self, app_name):
    try:
      print ("stopping app: " + app_name)
      if self.behavior_manager_proxy.isBehaviorRunning(app_name):
        self.behavior_manager_proxy.stopBehavior(app_name)
      else:
        print ("app not running: " + app_name)
    except Exception as e:
      print (e)
    pass

class event_starter_module(object):
  def __init__(self, name):
    self.memory_proxy = ALProxy("ALMemory", CONFIGURE.NAO_ALPROXY_IP, CONFIGURE.NAO_ALPROXY_PORT)
    pass

  def start(self, evt_name, p):
    try:
      '''if p:
        print_p=p.encode('utf8')
        print "~~~raising event: "+str(evt_name)+" - "+print_p
      else:
        print "~~~raising event: "+str(evt_name)+" - (null)"
      '''
      
      self.memory_proxy.raiseEvent(str(evt_name), p)
    except Exception as e:
      print (e)
    pass

class RandomMoveControlModule(object):
    def __init__(self, name):
      self.bmProxy = ALProxy("ALBehaviorManager", CONFIGURE.NAO_ALPROXY_IP, CONFIGURE.NAO_ALPROXY_PORT)
      self.postureProxy = ALProxy("ALRobotPosture", CONFIGURE.NAO_ALPROXY_IP, CONFIGURE.NAO_ALPROXY_PORT)

      #self.behavior_manager_proxy = ALProxy("ALBehaviorManager", CONFIGURE.NAO_ALPROXY_IP, CONFIGURE.NAO_ALPROXY_PORT)
      pass

    def runBR(self, loop=3):
        random.seed(time.time())

        '''
                              "Stand/Gestures/Explain_1",
                              "Stand/Gestures/Explain_2",
                              "Stand/Gestures/Explain_3",
                              "Stand/Gestures/Explain_4",
                              "Stand/Gestures/Explain_5",
                              "Stand/Gestures/Explain_6",
                              "Stand/Gestures/Explain_7",
                              "Stand/Gestures/Explain_8"
        '''
        '''
        self.behavior_list = ["Stand/BodyTalk/Speaking/BodyTalk_1",
                              "Stand/BodyTalk/Speaking/BodyTalk_2",
                              "Stand/BodyTalk/Speaking/BodyTalk_3",
                              "Stand/BodyTalk/Speaking/BodyTalk_4",
                              "Stand/BodyTalk/Speaking/BodyTalk_5",
                              "Stand/BodyTalk/Speaking/BodyTalk_6",
                              "Stand/BodyTalk/Speaking/BodyTalk_7",
                              "Stand/BodyTalk/Speaking/BodyTalk_8",
                              "Stand/BodyTalk/Speaking/BodyTalk_9",
                              "Stand/BodyTalk/Speaking/BodyTalk_10",
                              "Stand/BodyTalk/Speaking/BodyTalk_11",
                              "Stand/BodyTalk/Speaking/BodyTalk_12",
                              "Stand/BodyTalk/Speaking/BodyTalk_13",
                              "Stand/BodyTalk/Speaking/BodyTalk_14",
                              "Stand/BodyTalk/Speaking/BodyTalk_15",
                              "Stand/BodyTalk/Speaking/BodyTalk_16"]
        '''        
        self.behavior_list = [
                              "Stand/BodyTalk/Speaking/BodyTalk_2",
                              "Stand/BodyTalk/Speaking/BodyTalk_3",
                              "Stand/BodyTalk/Speaking/BodyTalk_4",
                              "Stand/BodyTalk/Speaking/BodyTalk_5",
                              "Stand/BodyTalk/Speaking/BodyTalk_6",
                              "Stand/BodyTalk/Speaking/BodyTalk_7",
                              "Stand/BodyTalk/Speaking/BodyTalk_8",
                              "Stand/BodyTalk/Speaking/BodyTalk_9",
                              "Stand/BodyTalk/Speaking/BodyTalk_10",
                              "Stand/BodyTalk/Speaking/BodyTalk_11",
                              "Stand/BodyTalk/Speaking/BodyTalk_12",
                              "Stand/BodyTalk/Speaking/BodyTalk_14",
                              "Stand/BodyTalk/Speaking/BodyTalk_15",
                              "Stand/BodyTalk/Speaking/BodyTalk_16"]

        beh_list = []
        if self.bmProxy:
          counter = loop 
          while counter:
              self.selected_behavior1 = random.choice(self.behavior_list)
              beh_list.append(self.selected_behavior1)
              counter -= 1

          counter = loop 
          while counter:
              self.bmProxy.post.runBehavior(beh_list[counter-1])
              counter -= 1

        '''
        self.selected_behavior2 = random.choice(self.behavior_list)
        if self.bmProxy:
            if not self.bmProxy.isBehaviorRunning(self.selected_behavior2):
                self.bmProxy.post.runBehavior(self.selected_behavior2)
        '''
        #self.postureProxy.post.goToPosture("Stand", 1.0)

        pass

    def stand(self,duration):
        self.postureProxy.post.goToPosture("Stand", duration)
        pass

NAO_DANCE_LIST = ["twinkle-twinkle-little-star"]

def random_dance_for_fun(app_starter):
  app_starter.start(random.choice(NAO_DANCE_LIST))

DANCE_TEXT_PREPARE = {CONFIGURE.LANG_CN: "要我跳个舞，是吗", CONFIGURE.LANG_CT: "要我跳個舞，是嗎", CONFIGURE.LANG_EN: "want me to dance"}
DANCE_TEXT_OK = {CONFIGURE.LANG_CN: "我跳舞啦", CONFIGURE.LANG_CT: "我跳舞啦", CONFIGURE.LANG_EN: "I will dance"}
DANCE_TEXT_PITY = {CONFIGURE.LANG_CN: "可惜不能看我跳舞，下次吧", CONFIGURE.LANG_CT: "可惜不能看我跳舞，下次吧", CONFIGURE.LANG_EN: "oh well. You will miss the fun. next time then"}

def go_to_dance(app_starter, record_thread, tts_thread, led, current_lang):
  # TTS process
  led.eyes_fade(USER_CONFIGURE.NAO_LED_COLOR_TTS_PROCESS)
  say_text(tts_thread, DANCE_TEXT_PREPARE[current_lang], current_lang)
  # STT process
  led.eyes_fade(USER_CONFIGURE.NAO_LED_COLOR_ASR_PROCESS)
  text = listen_text(record_thread, current_lang)
  text = text.lower()
  if "是的" in text or "yes" in text:
    # TTS process
    led.eyes_fade(USER_CONFIGURE.NAO_LED_COLOR_TTS_PROCESS)
    say_text(tts_thread, DANCE_TEXT_OK[current_lang], current_lang)
    random_dance_for_fun(app_starter)
  else:
    # TTS process
    led.eyes_fade(USER_CONFIGURE.NAO_LED_COLOR_TTS_PROCESS)
    say_text(tts_thread, DANCE_TEXT_PITY[current_lang], current_lang)

if __name__ == "__main__":
  app_starter = app_starter_module("app_starter")
  app_starter.start("twinkle-twinkle-little-star")
