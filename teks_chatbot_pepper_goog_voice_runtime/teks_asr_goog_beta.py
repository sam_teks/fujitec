# -*- coding: utf8 -*-

import sys
reload(sys)
sys.setdefaultencoding('utf8')

import os
import time
import pyaudio
import logging

import teks_configure as CONFIGURE
import multi_stt

#from msc_wrapper import *
import speech_recognition_teks as sr

from teks_led_state import led_state
from teks_audio_player import audio_player

#=============================================================

logging.basicConfig(level = logging.INFO)

################################################################################

import threading

class teks_record_thread(threading.Thread):
    def __init__(self, stop_event, rlock, lang="cmn-Hans-HK", idx=0):
        global sr

        threading.Thread.__init__(self)
        
        self.thread_stop_event = stop_event
        self.thread_rlock = rlock
        
        self.thread_stop = False
        self.suspend_flag = True
        self.run_beh_suspend_flag = False
        
        self.record_device_index = idx

        self.recognizer = sr.Recognizer()
        self.recognizer.set_lock(rlock)
        self.audio = None

        self.lang = lang
        #self.lang = "yue-Hant-HK"
        #self.lang = "zh-CN"
        #self.lang = "cmn-Hans-HK"
        #self.lang = "zh-HK"
        #self.lang = "en-US"

        self.speech_lang = CONFIGURE.LANG_CT

        self.said_text = ""
        self.result = ""

        self.led = led_state()
        self.aplayer = audio_player()
        self.curPath = os.path.dirname(os.path.abspath(__file__))    

        #self.ifly.login()    # When use with yuyi, comment it please.

    def suspend(self):
        with self.thread_rlock:
            #print('suspend google beta')
            self.suspend_flag = True
        
    def suspend_beh(self):
        with self.thread_rlock:
            #print('suspend beh beta')
            self.recognizer.suspend()
            self.run_beh_suspend_flag = True
            self.suspend_flag = True
        
    def resume(self):
        with self.thread_rlock:
            #print('resume google beta')
            self.suspend_flag = False
            self.result = ""
            
    def resume_beh(self):
        with self.thread_rlock:
            #print('resume beh beta')
            self.recognizer.resume()
            self.run_beh_suspend_flag = False
            self.suspend_flag = False
            self.result = ""
            
    def is_suspend(self):
        return (self.suspend_flag or self.run_beh_suspend_flag)

    def set_input(self, lang):
        self.lang = lang
            
    def get_output(self):
        return self.result
        
    def get_speech_lang(self):
        return self.speech_lang
       
    def run(self):
        global sr

        #init
        self.recognizer.energy_threshold = 1000
        
        #add multi-lang object
        self.recog = multi_stt.MultiRecognition()

        while not self.thread_stop_event.is_set():
            if self.suspend_flag or self.run_beh_suspend_flag:
                time.sleep(0.1)
                continue
            
            ##self.recognizer.energy_threshold = 2500
            # do something          
            with sr.Microphone(self.record_device_index, 16000, 1024) as source:
                ###self.recognizer.adjust_for_ambient_noise(source) #AlanYan
                
                #logging.info("checked minimum energy threshold to {}".format(self.recognizer.energy_threshold))
                logging.info("Say Something!")

                with self.thread_rlock:
                    self.led.eyes_fade(CONFIGURE.NAO_LED_COLOR_ASR_PROCESS)
                    #self.aplayer.play(self.curPath+"/begin_reco.wav")
                
                self.audio, energy, overtime = self.recognizer.listen(source=source, reclength=7)
                
                logging.info("Audio Length: " + str(len(self.audio.get_flac_data())))
                
                self.audio.save_wav_file()
                
                with self.thread_rlock:
                    self.led.eyes_fade(CONFIGURE.NAO_LED_COLOR_PROCESS_PROCESS)
                    self.aplayer.play(self.curPath+"/end_reco.wav")

                #langs = ["yue-Hant-HK","cmn-Hans-HK","en-US"]
                #speech_langs = {"yue-Hant-HK":CONFIGURE.LANG_CT,"cmn-Hans-HK":CONFIGURE.LANG_CN,"en-US":CONFIGURE.LANG_CT}
                #langs = ["yue-Hant-HK","cmn-Hans-HK"]
                #speech_langs = {"yue-Hant-HK":CONFIGURE.LANG_CT,"cmn-Hans-HK":CONFIGURE.LANG_CN}
                #langs = ["yue-Hant-HK"]
                #speech_langs = {"yue-Hant-HK":CONFIGURE.LANG_CT}
                
                with self.thread_rlock:
                    if self.suspend_flag or self.run_beh_suspend_flag:
                        self.result = ""
                        self.confidence = 0
                        continue

                try:
                    start_time = time.time()
                    #self.result will be a dict like:
                    #       {'confidence': 0.99999, 'transcript': 'hello', 'lang_code':'yue-Hant-HK'}
                    
                    self.result_set = self.recog.multiThreading(self.recognizer.recognize_google, self.audio, CONFIGURE.SPEECH_GOOGLE_LANGS)
                    
                    logging.info("STT Time: " + str(time.time() - start_time))
                    
                    if "confidence" in self.result_set and self.result_set["confidence"] > 0:
                        print("has result")
                        self.result = self.result_set["transcript"]
                        self.confidence = self.result_set["confidence"]
                        self.speech_lang = CONFIGURE.SPEECH_RECOG_LANGS[self.result_set["lang_code"]]
                        
                        self.recognizer.energy_threshold=energy
                        print "|+|+|+|+|+|+|+| self.recognizer.energy_threshold set to "+str(energy)
                    else:
                        print("here no result")
                        self.result = ""
                        self.confidence = 0
                        
                        if overtime:
                            self.recognizer.energy_threshold=self.recognizer.energy_threshold+1000
                            print "|+|+|+|+|+|+|+| self.recognizer.energy_threshold rise to "+str(self.recognizer.energy_threshold)

                    self.suspend_flag = True

                    '''                
                    start_time = time.time()

                    self.said_text, self.confidence1 = self.recognizer.recognize_google(self.audio, None, self.lang)
                    if self.said_text is None: self.said_text = ""
                    self.confidence = self.confidence1
                    self.speech_lang = CONFIGURE.LANG_CT
                    
                    logging.info("RECOGNIZED2: " + self.said_text.encode("utf8") + " " + str(self.confidence1))
                    logging.info("Record Time: " + str(time.time() - start_time))

                    if self.lang != "cmn-Hans-HK":
                        start_time = time.time()

                        self.said_text2, self.confidence2 = self.recognizer.recognize_google(self.audio, None, "cmn-Hans-HK")
                        if self.said_text2 is None: self.said_text2 = ""

                        logging.info("RECOGNIZED in Mandarin: " + self.said_text2.encode("utf8") + " " + str(self.confidence2))
                        logging.info("Record Time: " + str(time.time() - start_time))

                        if self.confidence2 > self.confidence1 and self.confidence2 > 0.85:
                                self.said_text = self.said_text2
                                self.confidence = self.confidence2
                                self.speech_lang = CONFIGURE.LANG_CN

                    logging.info("Selected text and confidence: " + self.said_text.encode("utf8") + " " + str(self.confidence))
                    
                    self.result = self.said_text
                    self.suspend_flag = True
                    '''

                except sr.UnknownValueError:
                    logging.info("Sorry, not understand i goog beta")
                    self.result = ""
                    self.confidence = 0
                    self.suspend_flag = True
                    
                except sr.RequestError as e:
                    logging.info("No connection")
                    self.result = ""
                    self.confidence = 0
                    self.suspend_flag = True
                    
                except KeyboardInterrupt:
                    self.suspend_flag = True
                    self.result = ""
                    self.confidence = 0
                    logging.info("KeyboardInterrupt within listen loop")
                    pass
        
        # Logout iflyteks asr           
        #self.ifly.logout()    # When use with yuyi, comment it please.

###############################################################

if __name__ == "__main__":
    
    pill2kill = threading.Event()
    rlock = threading.RLock()
    
    record_thread = teks_record_thread(1, "record_thread_1", pill2kill, rlock)
    record_thread.start()
    
    logging.info('\n*** INITIALIZE SUCCESS ***\n')
    
    try:
        while True:
            try:
                record_thread.resume()
                while not record_thread.is_suspend(): time.sleep(0.1)
                record_thread.suspend()
                text_result = record_thread.get_output()
                logging.info("HEARD: " + text_result)
        
            except Exception as e:
                print e
                pill2kill.set()
                pass

    except Exception as e:
        print e
        
    finally:
        print '\n*** WAIT FOR CHILD THREADS EXIT ***\n'
        
        pill2kill.set()
        record_thread.join()
    
        print '\n*** FINISH ***\n'
