#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
CONFIGURE_FILE_ABSOLUTE_PATH = os.path.split(os.path.realpath(__file__))[0]


# Set neo4j authorize parameter
#this is jron's id ,need change by yours.
NEO4J_AUTHORIZE_PARAM_LOCAL = ["http://127.0.0.1:7474", "neo4j", "neo4j"]
#this is jron's id ,need change into cloud os id.
NEO4J_AUTHORIZE_PARAM_DIALOG = ["http://111.231.102.179:7472", "neo4j", "tek123456"]

# this is for MacauWater project
#SEARCH_NODE_SENTENCE = 'MATCH (n:{})-[:HAS]->(m:QNA) ' \
#                       'where m.language="{}" AND m.question="{}" return m.answer as Answer'
# SEARCH_NODE_SENTENCE = 'match (n:{})-[:HAS]->(m) where m.language="{}" ' \
                       # 'AND "{}" contains m.question return m.answer as Answer'
SEARCH_NODE_SENTENCE = 'With "{}" As myQue MATCH (n:{})-[:HAS]-(m) ' \
                       'where m.lang="{}" and ' \
                       'filter(x IN m.kw1 WHERE myQue contains x) and ' \
                       'filter(x IN m.kw2 WHERE myQue contains x) and ' \
                       'filter(x IN m.kw3 WHERE myQue contains x) and ' \
                       'filter(x IN m.kw4 WHERE myQue contains x) and ' \
                       'filter(x IN m.kw5 WHERE myQue contains x) ' \
                       'return m.que as Question ,m.ans as Answer ,m.kwno as NoOfKeywords,size(m.que) as QuestionLength'

INSERT_NODE_SENTENCE = 'MATCH (n: %s) return n"'
# INSERT_NODE_SENTENCE = 'MATCH (n: %s) CREATE (m: %s' \
                       # '{language:"%s",question:"%s",answer:"%s",Seeded:"%s"}), (n)-[:HAS]->(m)'
                   # # (domain, subCaption, lang, question_text, answer_text, seed))

FILE_NAME = "app_config.ini"

LANG_CT = "CT"
LANG_EN = "EN"
LANG_CN = "CN"

LANGUAGE_EN = "en_us"
LANGUAGE_CN = "zh_cn"
#LANGUAGE_CT = "zh_hk"

SUPPORT_LANGUAGE_EN = True
SUPPORT_LANGUAGE_CN = True
#SUPPORT_LANGUAGE_CT = True
