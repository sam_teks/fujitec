
#!/usr/bin/env python
#  -*- coding: utf-8 -*-

from py2neo import Graph, authenticate

import teks_dialog_configure as CONFIGURE

##################################################################

def authorize_dialog():
    '''
    graph_url = "http://111.231.102.179:7474" #should read from OS environ
    username = "neo4j"  #should read from OS environ
    password = "tek123456"  #should read from OS environ
    bolt should be openen
    set bolt_port = 7687
    '''
    [graph_url, username, password] = CONFIGURE.NEO4J_AUTHORIZE_PARAM_DIALOG
    graph = None
    try:
        if username and password:
            authenticate(graph_url.strip("http://"), username, password)
        graph = Graph(graph_url + "/db/data", bolt=True, bolt_port =7685)
    except Exception as e:
        print e
        graph = None
    return graph

def authorize_local():
    '''
    graph_url = "http://127.0.0.1:7474" #should read from OS environ
    username = "neo4j"  #should read from OS environ
    password = "nao"  #should read from OS environ
    '''
    [graph_url, username, password] = CONFIGURE.NEO4J_AUTHORIZE_PARAM_LOCAL
    
    graph = None
    try:
        if username and password:
            authenticate(graph_url.strip("http://"), username, password)
        graph = Graph(graph_url + "/db/data")
    except Exception as e:
        print e
        graph = None
    return graph
