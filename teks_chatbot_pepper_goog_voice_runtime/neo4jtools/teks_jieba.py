#!/usr/bin/env python
#  -*- coding:utf-8 -*-

import time
import jieba
import jieba.analyse
import unicodecsv

class jieba_module(object):
    def __init__(self):
        start_time = time.time()
        jieba.initialize()
        jieba.load_userdict("./extra_dict/dict_hkia.txt.big")
        with open("./extra_dict/jieba_words_user.txt", 'rb') as f:
            reader = unicodecsv.reader(f, encoding='utf-8')
            for rows in reader:
                jieba.add_word(rows[0])

        jieba.analyse.set_stop_words("./extra_dict/stop_words_user.txt")
        jieba.analyse.set_idf_path("./extra_dict/idf_hkia.txt.big");

        print ("jieba init and load files time: %s" % str(time.time() - start_time))

    def extract_tags(self, input_text):
        '''
        - assume jieba is initialized already to shorten the loading time.
        - assume stop words, user dictionary, idf are loaded first.

        inputs
        - text string to be extracted

        outputs
        - list of keywords extracted by Jieba
        '''
        start_time = time.time()
        result = jieba.analyse.extract_tags(input_text)
        strip_result = [ x for x in result if x != " " ]
        print ("jieba time: " + str(time.time() - start_time))
        return strip_result

################################################################################

from teks_my_thread import teks_thread

class teks_jieba_thread(teks_thread):
    def __init__(self, stop_event, rlock):
        teks_thread.__init__(self, stop_event, rlock)

        self.jieba = jieba_module()
        self.text = ""
        self.result = []

    def set_input(self, text = ""):
        self.text = text

    def get_output(self):
        return self.result

    def run(self):
        while not self.thread_stop_event.is_set():
            if self.suspend_flag:
                time.sleep(0.1)
                continue

            # do something
            self.result = self.jieba.extract_tags(test_text)

            self.suspend_flag = True

################################################################################

import threading

if __name__ == "__main__":
    pill2kill = threading.Event()
    rlock = threading.RLock()

    jieba_thread = teks_jieba_thread(pill2kill, rlock)
    jieba_thread.start()

    test_text = "你好，我的名字是费玉清，我来自台湾，那么，你来自哪里呢？"
    jieba_thread.set_input(test_text)
    jieba_thread.resume()
    while not jieba_thread.is_suspend(): time.sleep(0.01)
    jieba_thread.suspend()
    strip_result = jieba_thread.get_output()
    for word in strip_result:
        print word

    pill2kill.set()
    jieba_thread.join()
