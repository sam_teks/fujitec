<?xml version="1.0" encoding="UTF-8" ?>
<Package name="tp_ct_a_fujitec_tradeplatform" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="main" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="angular-PapaParse" src="html/js/angular-PapaParse.js" />
        <File name="angular-fastclick.min" src="html/js/angular-fastclick.min.js" />
        <File name="angular-touch" src="html/js/angular-touch.js" />
        <File name="awesomplete" src="html/js/awesomplete.js" />
        <File name="awesomplete.min" src="html/js/awesomplete.min.js" />
        <File name="bootstrap" src="html/js/bootstrap.js" />
        <File name="jquery-2.1.4.min" src="html/js/jquery-2.1.4.min.js" />
        <File name="jquery.mobile.custom.min" src="html/js/jquery.mobile.custom.min.js" />
        <File name="main_camera" src="html/js/main_camera.js" />
        <File name="npm" src="html/js/npm.js" />
        <File name="papaparse.min" src="html/js/papaparse.min.js" />
        <File name="plyr" src="html/js/plyr.js" />
        <File name="robotutils.1.0" src="html/js/robotutils.1.0.js" />
        <File name="robotutils.min" src="html/js/robotutils.min.js" />
        <File name="videoutils" src="html/js/videoutils.js" />
        <File name="videoutils.min" src="html/js/videoutils.min.js" />
        <File name="sample" src="html/json_data/sample.json" />
        <File name="" src="html/.DS_Store" />
        <File name="01_applause" src="html/01_applause.ogg" />
        <File name="FutuMd" src="html/app/css/FutuMd.ttf" />
        <File name="MSblack" src="html/app/css/MSblack.ttf" />
        <File name="btnconfig" src="html/app/css/btnconfig.css" />
        <File name="common" src="html/app/css/common.css" />
        <File name="style" src="html/app/css/style.css" />
        <File name="style_plus" src="html/app/css/style_plus.css" />
        <File name="action" src="html/app/js/action.js" />
        <File name="click" src="html/click.ogg" />
        <File name="click" src="html/click.wav" />
        <File name="bootstrap.min" src="html/css/bootstrap.min.css" />
        <File name="font-awesome.min" src="html/css/font-awesome.min.css" />
        <File name="plyr" src="html/css/plyr.css" />
        <File name="quiz" src="html/data/quiz.py" />
        <File name="quizQuestion" src="html/data/quizQuestion.json" />
        <File name="DINRoundOffcPro-Black" src="html/fonts/DINRoundOffcPro-Black.ttf" />
        <File name="DINRoundOffcPro-Bold" src="html/fonts/DINRoundOffcPro-Bold.ttf" />
        <File name="DINRoundOffcPro-Light" src="html/fonts/DINRoundOffcPro-Light.ttf" />
        <File name="DINRoundOffcPro-Medium" src="html/fonts/DINRoundOffcPro-Medium.ttf" />
        <File name="DINRoundOffcPro" src="html/fonts/DINRoundOffcPro.ttf" />
        <File name="FontAwesome" src="html/fonts/FontAwesome.otf" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.eot" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.svg" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.ttf" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.woff" />
        <File name="fontawesome-webfont" src="html/fonts/fontawesome-webfont.woff2" />
        <File name="BG" src="html/img/BG.jpg" />
        <File name="FSCloseBtn" src="html/img/FSCloseBtn.png" />
        <File name="face" src="html/img/face.png" />
        <File name="face1" src="html/img/face1.png" />
        <File name="imgConfig" src="html/img/imgConfig.js" />
        <File name="p1" src="html/img/main/p1.png" />
        <File name="transparent" src="html/img/main/transparent.png" />
        <File name="menu_option1" src="html/img/menu_option1.png" />
        <File name="menu_option2" src="html/img/menu_option2.png" />
        <File name="menu_option2_old" src="html/img/menu_option2_old.png" />
        <File name="recording" src="html/img/recording.gif" />
        <File name="index" src="html/index.html" />
        <File name="angular-PapaParse.min" src="html/js/angular-PapaParse.min.js" />
        <File name="angular-route.min" src="html/js/angular-route.min.js" />
        <File name="angular-touch.min" src="html/js/angular-touch.min.js" />
        <File name="angular.min" src="html/js/angular.min.js" />
        <File name="bootstrap.min" src="html/js/bootstrap.min.js" />
        <File name="jquery-1.11.0.min" src="html/js/jquery-1.11.0.min.js" />
        <File name="jquery-3.1.1.min" src="html/js/jquery-3.1.1.min.js" />
        <File name="js.cookie" src="html/js/js.cookie.js" />
        <File name="main" src="html/js/main.js" />
        <File name="moment.min" src="html/js/moment.min.js" />
        <File name="robotutils" src="html/js/robotutils.js" />
        <File name="robotutils_2.0" src="html/js/robotutils_2.0.js" />
        <File name="miscconfig" src="html/app/css/miscconfig.css" />
        <File name="slider.min" src="html/css/slider.min.css" />
        <File name="directory" src="html/directory.txt" />
        <File name="b1" src="html/img/main/b1.jpg" />
        <File name="b1b1" src="html/img/main/b1b1.jpg" />
        <File name="b1b1b1" src="html/img/main/b1b1b1.jpg" />
        <File name="b1b1b1p1" src="html/img/main/b1b1b1p1.jpg" />
        <File name="b1b1b2" src="html/img/main/b1b1b2.jpg" />
        <File name="b1b1b2p1" src="html/img/main/b1b1b2p1.jpg" />
        <File name="b1b1b3b1" src="html/img/main/b1b1b3b1.jpg" />
        <File name="b1b1b3b1b1" src="html/img/main/b1b1b3b1b1.jpg" />
        <File name="b1b1b3b1b1p1" src="html/img/main/b1b1b3b1b1p1.jpg" />
        <File name="b1b1b3b2" src="html/img/main/b1b1b3b2.jpg" />
        <File name="b1b1b3b3" src="html/img/main/b1b1b3b3.jpg" />
        <File name="b1b1b3b4" src="html/img/main/b1b1b3b4.jpg" />
        <File name="b1b1b3b5" src="html/img/main/b1b1b3b5.jpg" />
        <File name="b1b1b3b6" src="html/img/main/b1b1b3b6.jpg" />
        <File name="1" src="html/img/slideshow/1.jpg" />
        <File name="2" src="html/img/slideshow/2.jpg" />
        <File name="3" src="html/img/slideshow/3.jpg" />
        <File name="4" src="html/img/slideshow/4.jpg" />
        <File name="angular-swiper" src="html/js/angular-swiper.js" />
        <File name="slider.min" src="html/js/slider.min.js" />
        <File name="special_2017-jctv_fullversion_1200kbps" src="html/videos/special_2017-jctv_fullversion_1200kbps.mp4" />
        <File name="special_2017-racingtouch_20170306v3_1200kbps" src="html/videos/special_2017-racingtouch_20170306v3_1200kbps.mp4" />
        <File name="config1" src="html/config1.csv" />
        <File name="config" src="html/img/slideshow/config.csv" />
    </Resources>
    <Topics />
    <IgnoredPaths />
    <Translations auto-fill="en_US">
        <Translation name="translation_en_US" src="translations/translation_en_US.ts" language="en_US" />
        <Translation name="translation_zh_CN" src="translations/translation_zh_CN.ts" language="zh_CN" />
        <Translation name="translation_zh_TW" src="translations/translation_zh_TW.ts" language="zh_TW" />
    </Translations>
</Package>
